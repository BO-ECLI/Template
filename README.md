# BO-ECLI Parser Template project for creating a new national extension

The BO-ECLI Parser Engine analyzes the text of legal documents and identifies legal references.
The software can be extended in order to support new languages and jurisdictions through Java Service Provider Interface (https://docs.oracle.com/javase/tutorial/ext/basics/spi.html).
The main technical issues of the BOECLI Parser Engine are discussed in the repository wiki (https://gitlab.com/BO-ECLI/Engine/wikis/home), check it out before proceeding. You can also have a look at the Italian extension (https://gitlab.com/BO-ECLI/it) that has been realized following the Template project.

In order to realize a new national extension, follow these steps:

1. Import the Template project.
2. Make the necessary adjustments to package names.
3. Edit the `xy.boecli.impl.Config`.
4. Extend the common lists of normalized values contained in the Engine with specific national values. In particular, extend the lists of normalized values for:
	- national case-law authorities (for national case-law authorities values, it is recommended to start with the country code, an underscore and a court code, all in capital letters; if available, the court code should be the ECLI court code, e.g. "IT\_CASS");
	- national legislation authorities (for national legislation authorities values, it is recommended to start with the country code, an underscore and a court code, all in capital letters, e.g. "IT\_PRES\_REP");
	- national legislation aliases (for national legislation aliases values, it is recommended to start with the country code, an underscore and an abbreviated and normalized value of the alias, e.g. "IT\_COD\_CIV").
5. and, optionally, evaluate the extension of the lists of normalized values for:
	- case-law aliases;
	- legislation, case-law and generic (legal) types of document;
	- national identifiers.
6. The next steps involve the use of JFlex for implementing most of the services. Download the binary and have a look at the user manual starting from the official site (http://www.jflex.de).
7. Sets of macros of regular expressions can be included and shared among several _jflex_ files. Configure the national and language dependent ones:
	- the `NationalMacros.in` file, containing the declarations of prepositions and common prefixes expressed in the language of the new implementation;
	- the `GeographicMacros.in` file, containing the declarations of European countries, national regions and cities, expressed in the language of the new implementation.
8. Implement as many ***entity identification services*** as you need. Start by following the pipeline already present in the Template:
	- aliases identification
	- geographic identification
	- sections identification
	- european courts identification
	- high courts identification
	- other courts identification
	- authorities identification
	- types identification
	- subjects identification
	- dates identification
	- case numbers identification
	- numbers identification
	- partition elements identification
	- stopwords identification.
Check the wiki at https://gitlab.com/BO-ECLI/Engine/wikis/home to know more about specific identification services and the elements involved.	
9. The BOECLI Engine provides several default reference recognition service implementations that are able to cover the most typical patterns for legal references. If specific national patterns remain uncovered, implement one or more ***reference recognition service***. 
10. Implement as many ***identifier generation services*** as possible in order to assign a standard identifier to the recognized references. Specifically, it is recommended to implement the identifier generation services for:
	- assigning an ECLI code to case-law references that involve national courts when it is possible to generate it by simply composing it with the elements identified within the reference;
	- associate a standard identifier for legislation to each legislation alias value (see step 4) in order to assign it to a reference that contains that alias value.
It is also possible to assign, along with the identifier code, an URL for the referred document and a score of confidence: a legal reference object can be associated with several legal identifier objects.
11. Compile the `.jflex` files into `.java` files with the following command (from the directory containing the jflex file):
`java -Xmx4096m -Dfile.encoding=UTF-8 -jar /path/to/jflex-1.6.1.jar -d . --nobak --verbose --noinputstreamctor ./File.jflex` 
12. Register the services in the `META-INF/services/eu.boecli.service.BOECLIService` file.
13. Export the project into a `.jar` library (i.e. the national library).
14. Debug and test the BOECLI Parser Engine with both the engine library and the national library visible in the *classpath*: 
	- debug it during development using the code reported in the README file of the BOECLI Engine project;
	- check the BOECLI Test project in order to do a complete test on the coverage accuracy with a test set of citations in the newly implemented language.


&nbsp;


