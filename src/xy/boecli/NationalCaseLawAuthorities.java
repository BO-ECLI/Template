package xy.boecli;

import eu.boecli.common.CaseLawAuthority;

public enum NationalCaseLawAuthorities implements CaseLawAuthority {

	
	NATIONAL_CASELAW_AUTHORITY_1,
	
	NATIONAL_CASELAW_AUTHORITY_2,
	
	XX_SUP,
	
	XX_COST,
	
	XX_AUDIT,
	
	XX_SC,
	
	XX_TRIB;
	
}
