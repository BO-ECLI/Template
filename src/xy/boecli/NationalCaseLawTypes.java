package xy.boecli;

import eu.boecli.common.CaseLawType;

public enum NationalCaseLawTypes implements CaseLawType {

	
	NATIONAL_CASELAW_TYPE_OF_DOCUMENT_1,
	
	NATIONAL_CASELAW_TYPE_OF_DOCUMENT_2;
	
	
}
