package xy.boecli;

import eu.boecli.common.LegalType;

public enum NationalLegalTypes implements LegalType {

	
	NATIONAL_LEGAL_TYPE_OF_DOCUMENT_1,
	
	NATIONAL_LEGAL_TYPE_OF_DOCUMENT_2;
	
	
}
