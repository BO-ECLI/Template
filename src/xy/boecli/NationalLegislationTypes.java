package xy.boecli;

import eu.boecli.common.LegislationType;

public enum NationalLegislationTypes implements LegislationType {

	
	NATIONAL_LEGISLATION_TYPE_OF_DOCUMENT_1,
	
	NATIONAL_LEGISLATION_TYPE_OF_DOCUMENT_2;
	
	
}
