package xy.boecli.impl;

import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;

public class Config {

	
	public final static Language LANG = null; //Language.XY;
	
	public final static Jurisdiction JUR = null; //Jurisdiction.XY;
	
	public final static String NAME = "Extension for XY language";
	
	public final static String AUTHOR = "XY Extension Author";
	
	public final static String VERSION = "0.1";
	
}
