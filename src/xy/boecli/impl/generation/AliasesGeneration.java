package xy.boecli.impl.generation;

import java.util.ArrayList;
import java.util.Collection;

import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.reference.LegalIdentifier;
import eu.boecli.reference.LegalIdentifierFactory;
import eu.boecli.reference.LegalReference;
import eu.boecli.service.generation.IdentifierGenerationService;
import xy.boecli.NationalIdentifiers;
import xy.boecli.NationalLegislationAliases;
import xy.boecli.impl.Config;

public class AliasesGeneration extends IdentifierGenerationService {

	@Override
	public Language language() {

		return Config.LANG;
	}

	@Override
	public Jurisdiction jurisdiction() {

		return Config.JUR;
	}

	@Override
	public String extensionName() {

		return Config.NAME;
	}

	@Override
	public String author() {

		return Config.AUTHOR;
	}

	@Override
	public String serviceName() {

		return "National Legislation Alias identifier generation service";
	}

	@Override
	public String version() {

		return Config.VERSION;
	}

	@Override
	protected Collection<LegalIdentifier> getLegalIdentifiers(LegalReference legalReference) throws IllegalArgumentException {

		Collection<LegalIdentifier> legalIdentifiers = new ArrayList<LegalIdentifier>();
		
		//TODO manage partitions		
		
		String code = "";
		
		switch(NationalLegislationAliases.valueOf(legalReference.getAliasValue())) {
		
		case NATIONAL_LEGISLATION_ALIAS_1 :
			code = "NATIONAL_LEGISLATION_ALIAS_1-identifier-code";
			break;

		case NATIONAL_LEGISLATION_ALIAS_2 :
			code = "NATIONAL_LEGISLATION_ALIAS_2-identifier-code";
			break;

		}
		
		if( !code.equals("")) {
			
			String url = "http://some.url";
			
			LegalIdentifier legalIdentifier = LegalIdentifierFactory.createLegalIdentifier(NationalIdentifiers.NATIONAL_IDENTIFIER_1, code);
			
			if(legalIdentifier != null) {
			
				legalIdentifier.setConfidence(0.0);
				legalIdentifier.setUrl(url);
				legalIdentifiers.add(legalIdentifier);
			}
		}
		
		return legalIdentifiers;
	}
	
}
