package xy.boecli.impl.generation;

import java.util.ArrayList;
import java.util.Collection;

import eu.boecli.common.CommonIdentifiers;
import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.reference.LegalIdentifier;
import eu.boecli.reference.LegalIdentifierFactory;
import eu.boecli.reference.LegalReference;
import eu.boecli.service.generation.IdentifierGenerationService;
import xy.boecli.impl.Config;

public class ECLIGeneration extends IdentifierGenerationService {

	@Override
	public Language language() {

		return Config.LANG;
	}

	@Override
	public Jurisdiction jurisdiction() {

		return Config.JUR;
	}

	@Override
	public String extensionName() {

		return Config.NAME;
	}

	@Override
	public String author() {

		return Config.AUTHOR;
	}

	@Override
	public String serviceName() {

		return "National ECLI identifier generation service";
	}

	@Override
	public String version() {

		return Config.VERSION;
	}

	@Override
	protected Collection<LegalIdentifier> getLegalIdentifiers(LegalReference legalReference) {

		Collection<LegalIdentifier> legalIdentifiers = new ArrayList<LegalIdentifier>();
		
		
		//TODO create a LegalIdentifier object with an ECLI code depending on the features of the legalReference

		String number = legalReference.getNumber();

		String year = legalReference.getYear();
		
		String auth = legalReference.getYear();
		
		if(number.equals("") || year.equals("") || auth.equals("")) {
			
			return null;
		}
		
		String code = "ECLI:XY:" + auth + ":" + year + ":" + number;
		String url = "http://some.url";
		
		LegalIdentifier legalIdentifier = LegalIdentifierFactory.createLegalIdentifier(CommonIdentifiers.ECLI, code);
		
		if(legalIdentifier != null) {

			legalIdentifier.setConfidence(0.0);
			legalIdentifier.setUrl(url);
			legalIdentifiers.add(legalIdentifier);
		}
		
		return legalIdentifiers;
	}

}
