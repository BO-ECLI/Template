package xy.boecli.impl.generation;

import java.util.Collection;

import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.reference.LegalIdentifier;
import eu.boecli.reference.LegalReference;
import eu.boecli.service.generation.IdentifierGenerationService;
import xy.boecli.impl.Config;

public class ELIGeneration extends IdentifierGenerationService {

	@Override
	public Language language() {

		return Config.LANG;
	}

	@Override
	public Jurisdiction jurisdiction() {

		return Config.JUR;
	}

	@Override
	public String extensionName() {

		return Config.NAME;
	}

	@Override
	public String author() {

		return Config.AUTHOR;
	}

	@Override
	public String serviceName() {

		return "National ELI identifier generation service";
	}

	@Override
	public String version() {

		return Config.VERSION;
	}

	@Override
	protected Collection<LegalIdentifier> getLegalIdentifiers(LegalReference legalReference) {

		
		
		
		//TODO create a LegalIdentifier object with an ELI code depending on the features of the legalReference

		
		
		
		
		return null;
	}
	
}
