/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package xy.boecli.impl.identification;

import java.io.IOException;
import java.io.StringReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.service.identification.EntityIdentificationService;
import xy.boecli.impl.Config;


%%
%class CaseNumbersIdentification 
%extends EntityIdentificationService
%standalone
%unicode
%caseless
%char
%line
%column
%public

%include ../StdMacros.in
%include ../BoecliMacros.in
%include ../NationalMacros.in

%{

	/* Custom java code */

	@Override
	public Language language() { return Config.LANG; }

	@Override
	public Jurisdiction jurisdiction() { return Config.JUR; }

	@Override
	public String author() { return Config.AUTHOR; }

	@Override
	public String extensionName() { return Config.NAME; }

	@Override
	public String serviceName() { return "Case numbers and years entity identification"; }

	@Override
	public String version() { return "0.0"; }
	

	/* An empty default constructor is required to comply with BOECLIService */
	
	public CaseNumbersIdentification() { }

	@Override
	public final boolean run() {
		
		try {
			
			yyreset(new StringReader(getEngineDocument().getAnnotatedText()));
			yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}
		
		return true;
	}

	private static final Pattern digits = Pattern.compile("\\d+");
	
	/* Use the auxiliary methods provided by the EntityIdentificationService to save the identified entities. */

	private void save() {
	
		save("");
	}	
	
	private void save(String prefix) {

		String text = yytext();
		
		Matcher matcher = digits.matcher(text);
		
		if( !matcher.find()) {
			
			//TODO error
			addText(text);
			return;
		}
		
		String num = text.substring(matcher.start(), matcher.end());

		String year = "";
		
		if(matcher.find()) {
			
			year = text.substring(matcher.start(), matcher.end());
		}
		
		addText(text.substring(0,1));
		addCaseNumber(prefix, num, year, text.substring(1, text.length()-1));
		yypushback(1);
	}
	
%} 


PREFIX_NUMBER = (number)|(num\.?)|(nr\.?)|(n\.?)

RC = ({PREFIX_NUMBER}{SE}?)?(r\.?{SE}?c\.?)

CASE = (case)

PREFIX_CASE = ({ISSUED}{SE}?({BY}{SE})?)?({IN}{SE})?{CASE}

NUMBERYEAR_SEP = {YEAR_PREFIX}|{SLASH_SEP}|{BACKSLASH_SEP}

NUMBERYEAR = ({PREFIX_NUMBER}{SE}?)?{DD}{SE}?{NUMBERYEAR_SEP}{SE}?{YEAR}


/* "C" prefix */

CASE_C1 = (c){SE}?{DH}{SE}?{NUMBERYEAR}
CASE_C2 = (c){SE}?{NUMBERYEAR}
CASE_C3 = (c){SE}?{SLASHES}{SE}?{NUMBERYEAR}
CASE_C = {CASE_C1}|{CASE_C2}|{CASE_C3}



%x BE 

%%

{NAD}{PREFIX_CASE}{SE}?({PREFIX_NUMBER}{SE}?)?{NUMBERYEAR}{ND} 	{ save(); }

{NAD}({PREFIX_CASE}{SE}?)?({PREFIX_NUMBER}{SE}?)?{CASE_C}{ND} { save("C"); }																	


{BOPEN}		{ 
				addText(yytext()); 
				yybegin(BE); 
			}

<BE> {

	{BCLOS}			{ 
						addText(yytext()); 
						yybegin(YYINITIAL); 
					}
	
	[^]				{ 
						addText(yytext()); 
					}
}


[^]					{ addText(yytext()); }


