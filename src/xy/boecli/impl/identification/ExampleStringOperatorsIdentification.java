package xy.boecli.impl.identification;

import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.service.identification.EntityIdentificationService;
import xy.boecli.impl.Config;

public class ExampleStringOperatorsIdentification extends EntityIdentificationService {

	@Override
	public Language language() {

		return Config.LANG;
	}

	@Override
	public Jurisdiction jurisdiction() {

		return Config.JUR;
	}

	@Override
	public String extensionName() {

		return Config.NAME;
	}

	@Override
	public String author() {

		return Config.AUTHOR;
	}

	@Override
	public String serviceName() {

		return "National Entity identification service with String operators";
	}

	@Override
	public String version() {

		return Config.VERSION;
	}

	@Override
	protected boolean run() {


		//Identify entities with simple string operations
		
		String entity = "entity1";
		
		String text = getEngineDocument().getAnnotatedText();
		
		int start = text.indexOf(entity);
		
		if(start > -1) {

			//Text before entity
			addText(text.substring(0, start));
			
			//Use the correct auxiliary method here
			addText(text.substring(start, start + entity.length()));
			
			//Text after entity
			addText(text.substring(start + entity.length()));
			
		} else {
			
			addText(text);
		}
		
		return true;
	}

	
	
}
