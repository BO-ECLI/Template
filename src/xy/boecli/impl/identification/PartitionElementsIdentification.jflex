/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package xy.boecli.impl.identification;

import java.io.IOException;
import java.io.StringReader;

import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.service.identification.EntityIdentificationService;
import xy.boecli.impl.Config;

%%
%class PartitionElementsIdentification 
%extends EntityIdentificationService
%standalone
%unicode
%caseless
%char
%line
%column
%public

%include ../StdMacros.in
%include ../BoecliMacros.in
%include ../NationalMacros.in



%{

	/* Custom java code */

	@Override
	public Language language() { return Config.LANG; }

	@Override
	public Jurisdiction jurisdiction() { return Config.JUR; }

	@Override
	public String author() { return Config.AUTHOR; }

	@Override
	public String extensionName() { return Config.NAME; }

	@Override
	public String serviceName() { return "National partitions identification"; }

	@Override
	public String version() { return "0.0"; }
	

	/* An empty default constructor is required to comply with BOECLIService */
	
	public PartitionElementsIdentification() { }

	@Override
	public final boolean run() {
		
		try {
			
			yyreset(new StringReader(getEngineDocument().getAnnotatedText()));
			yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}
		
		return true;
	}

	private int offset = 0;
	private int length = 0;
	
	private int offsetLatin = 0;
	private int lengthLatin = 0;
	private String textLatin = "";
	private String value = "";
	private String latin = "";
	
	private int prevState = YYINITIAL;


	private void inList() {
	
		offset++;
		
		if( offset >= length ) {
			yypushback(1);
			yybegin(YYINITIAL);
		}
		
		addText(yytext());
	}
	
	private void startPartition(int partitionState) {
	
		addText(yytext().substring(0,1));
		offset = 1;
		length = yylength();
		yypushback(length-1);
		yybegin(partitionState);
	}
	
	private void startLatin() {
	
		offsetLatin = 0;
		value = "";
		latin = "";
		lengthLatin = yylength();
		offset += lengthLatin;
		prevState = yystate();
		
		textLatin = yytext();
		yypushback(lengthLatin);
		yybegin(Latin);
	}
	
	
%} 


/****** Identification of articles, paragraphs, letters and items entities  *******/


/* Latin numerals (up to 25th) */

Lat1 = (semel)
Lat2 = (bis)
Lat3 = (ter)
Lat4 = (quater)
Lat5 = (quinquies)
Lat6 = (sexies)
Lat7 = (septies)
Lat8 = (octies)
Lat9 = (novies)
Lat10 = (decies)
Lat11 = (undecies)
Lat12 = (duodecies)
Lat13 = (ter[ ]?decies)
Lat14 = (quater[ ]?decies)
Lat15 = (quinquies[ ]?decies)|(quindecies)
Lat16 = (sexies[ ]?decies)|(sexdecies)
Lat17 = (septies[ ]?decies)|(septdecies)
Lat18 = (octies[ ]?decies)|(octodecies)
Lat19 = (novies[ ]?decies)|(novodecies)
Lat20 = (vicies)
Lat21 = (vicies[ ]?semel)|(unvicies)
Lat22 = (vicies[ ]?bis)|(duovicies)
Lat23 = (vicies[ ]?ter)|(tervicies)
Lat24 = (vicies[ ]?quater)|(quatervicies)
Lat25 = (vicies[ ]?quinquies)|(quinvicies)

Latin = {Lat1}|{Lat2}|{Lat3}|{Lat4}|{Lat5}|{Lat6}|{Lat7}|{Lat8}|{Lat9}|{Lat10}|{Lat11}|{Lat12}|{Lat13}|{Lat14}|{Lat15}|{Lat16}|{Lat17}|{Lat18}|{Lat19}|{Lat20}|{Lat21}|{Lat22}|{Lat23}|{Lat24}|{Lat25}


/* Numeric references for partitions with optional latin specification */
/* "2" "2 bis" "2 ter)" "2-quater" "3 - quinquies )" */

NumLatinSuffix = ({DH}{SE}?)?{Latin}({SE}?[\)])? 
NumLatin = {DD}({SE}?{NumLatinSuffix})?

Following = (seguent.)|(seg\.?)|(segg\.?)
FollowingSuffix = ({AND}{SE}?)?{Following}

/* Multiples: "1, 2 AND 3" */ 

NumLatinAndNumLatin = {NumLatin}{SE}?{AND}{SE}?{NumLatin}

NumLatinNumLatinAndNumLatin = {NumLatin}({SCE}{NumLatin})+({SE}?{AND}{SE}?{NumLatin})?

NumLatinList = ({NumLatin}|{NumLatinAndNumLatin}|{NumLatinNumLatinAndNumLatin})({SE}?{FollowingSuffix})?




/* Letter references for partitions with optional latin specification */
/* "a" "b)" "a-bis)"  */

Lett = [a-zA-Z][a-zA-Z]?

LettLatinSuffix = ({DH}{SE}?)?{Latin}
LettLatinNoBracket = {Lett}({SE}?{LettLatinSuffix})?
LettLatinBracket = {Lett}({SE}?{LettLatinSuffix})?({SE}?[\)])
LettLatin = {LettLatinNoBracket}|{LettLatinBracket}

/* Multiples: "a,b,c" or "a), b) AND c)" */

LettLatinAndLettLatin = {LettLatinBracket}{SE}?{AND}{SE}?{LettLatinBracket}

LettLatinLettLatin = {LettLatinNoBracket}({SCE}{LettLatinNoBracket})+

LettLatinLettLatinAndLettLatin = {LettLatinBracket}({SCE}{LettLatinBracket})+({SE}?{AND}{SE}?{LettLatinBracket})?

LettLatinListBracket = ({LettLatinBracket}|{LettLatinAndLettLatin}|{LettLatinLettLatinAndLettLatin})({SE}?{FollowingSuffix})?

LettLatinListNoBracket = ({LettLatinNoBracket}|{LettLatinLettLatin})({SE}?{FollowingSuffix})?



/**************/
/* Partitions */
/**************/
ARTICLE = (articol.)|(art\.?)|(artt\.) /* (article) */

COMMA = (comma)|(commi)					

LETTER = (letter.)|(let\.?)|(lett\.?)  /* (letter) */

PARAGRAPH = (paragraf.)|(par\.?)|(punto)|(punti)  /* (paragraph)|(point) */




%x BE articleList commaList paragraphList letterListBracket letterListNoBracket Latin  

%%


{NAD}{ARTICLE}{SE}?{NumLatinList}{NAD}				{ startPartition(articleList); }

{NAD}{COMMA}{SE}?{NumLatinList}{NAD}				{ startPartition(commaList); }

{NAD}{PARAGRAPH}{SE}?{NumLatinList}{NAD}			{ startPartition(paragraphList); }

{NAD}{LETTER}{SE}?{LettLatinListBracket}{NAD}		{ startPartition(letterListBracket); }

{NAD}{LETTER}{SE}?{LettLatinListNoBracket}{NAD}		{ startPartition(letterListNoBracket); }


<articleList>	{
	
	{ARTICLE}{SE}?{NumLatin}({SE}?{FollowingSuffix})? 		{ startLatin(); }
						
	{NumLatin}({SE}?{FollowingSuffix})?						{ startLatin(); }
						
	[^]		{ inList(); }
}

<commaList>	{
	
	{COMMA}{SE}?{NumLatin}({SE}?{FollowingSuffix})? 		{ startLatin(); }
						
	{NumLatin}({SE}?{FollowingSuffix})?						{ startLatin(); }
						
	[^]		{ inList(); }
}

<paragraphList>	{
	
	{PARAGRAPH}{SE}?{NumLatin}({SE}?{FollowingSuffix})? 	{ startLatin(); }
						
	{NumLatin}({SE}?{FollowingSuffix})?						{ startLatin(); }
						
	[^]		{ inList(); }
}


<letterListBracket>	{
	
	{LETTER}{SE}?{LettLatinBracket}({SE}?{FollowingSuffix})?	{ startLatin(); }
						
	{LettLatinBracket}({SE}?{FollowingSuffix})?					{ startLatin(); }

	[^]		{ inList(); }
}


<letterListNoBracket>	{
	
	{LETTER}{SE}?{LettLatinNoBracket}({SE}?{FollowingSuffix})? 		{ startLatin(); }
						
	{LettLatinNoBracket}({SE}?{FollowingSuffix})?					{ startLatin(); }
						
	[^]		{ inList(); }
}


<Latin>	{

	{DD}		{ 
					value = yytext();
					offsetLatin += yylength();
				}
				
	{Lett}		{ 
					value = yytext();
					offsetLatin += yylength();
				} 
	
	{Lat1}		{ latin = "SEMEL"; offsetLatin += yylength(); }
	{Lat2}		{ latin = "BIS"; offsetLatin += yylength(); }
	{Lat3}		{ latin = "TER"; offsetLatin += yylength(); }
	{Lat4}		{ latin = "QUATER"; offsetLatin += yylength(); }
	{Lat5}		{ latin = "QUINQUIES"; offsetLatin += yylength(); }
	{Lat6}		{ latin = "SEXIES"; offsetLatin += yylength(); }
	{Lat7}		{ latin = "SEPTIES"; offsetLatin += yylength(); }
	{Lat8}		{ latin = "OCTIES"; offsetLatin += yylength(); }
	{Lat9}		{ latin = "NOVIES"; offsetLatin += yylength(); }
	{Lat10}		{ latin = "DECIES"; offsetLatin += yylength(); }
	{Lat11}		{ latin = "UNDECIES"; offsetLatin += yylength(); }
	{Lat12}		{ latin = "DUODECIES"; offsetLatin += yylength(); }		
	{Lat13}		{ latin = "TERDECIES"; offsetLatin += yylength(); }
	{Lat14}		{ latin = "QUATERDECIES"; offsetLatin += yylength(); }
	{Lat15}		{ latin = "QUINQUIESDECIES"; offsetLatin += yylength(); }
	{Lat16}		{ latin = "SEXIESDECIES"; offsetLatin += yylength(); }
	{Lat17}		{ latin = "SEPTIESDECIES"; offsetLatin += yylength(); }
	{Lat18}		{ latin = "OCTIESDECIES"; offsetLatin += yylength(); }
	{Lat19}		{ latin = "NOVIESDECIES"; offsetLatin += yylength(); }
	{Lat20}		{ latin = "VICIES"; offsetLatin += yylength(); }
	{Lat21}		{ latin = "UNVICIES"; offsetLatin += yylength(); }
	{Lat22}		{ latin = "DUOVICIES"; offsetLatin += yylength(); }
	{Lat23}		{ latin = "TERVICIES"; offsetLatin += yylength(); }
	{Lat24}		{ latin = "QUATERVICIES"; offsetLatin += yylength(); }
	{Lat25}		{ latin = "QUINQUIESVICIES"; offsetLatin += yylength(); }

	[^]			{
					offsetLatin++;
					
					if( offsetLatin >= lengthLatin ) {
						
						if(prevState == articleList) { addArticle(value, latin, textLatin); }
						if(prevState == commaList) { addComma(value, latin, textLatin); }
						if(prevState == paragraphList) { addParagraph(value, latin, textLatin); }
						if(prevState == letterListBracket || prevState == letterListNoBracket) { addLetter(value, latin, textLatin); }
						
						if( offsetLatin > lengthLatin ) { yypushback(1); }
							
						yybegin(prevState);
					}
				}
}




{BOPEN}		{ 
				addText(yytext()); 
				yybegin(BE); 
			}

<BE> {

	{BCLOS}			{ 
						addText(yytext()); 
						yybegin(YYINITIAL); 
					}
	
	[^]				{ 
						addText(yytext()); 
					}
}

[^]					{
						addText(yytext());
					}


