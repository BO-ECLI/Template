/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package xy.boecli.impl.identification;

import java.io.IOException;
import java.io.StringReader;

import eu.boecli.common.CommonCaseLawTypes;
import eu.boecli.common.CommonLegalTypes;
import eu.boecli.common.CommonLegislationTypes;
import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.common.Type;
import eu.boecli.service.identification.EntityIdentificationService;
import xy.boecli.impl.Config;

%%
%class TypesIdentification 
%extends EntityIdentificationService
%standalone
%unicode
%caseless
%char
%line
%column
%public

%include ../StdMacros.in
%include ../BoecliMacros.in
%include ../NationalMacros.in

%{

	/* Custom java code */

	@Override
	public Language language() { return Config.LANG; }

	@Override
	public Jurisdiction jurisdiction() { return Config.JUR; }

	@Override
	public String author() { return Config.AUTHOR; }

	@Override
	public String extensionName() { return Config.NAME; }

	@Override
	public String serviceName() { return "National Types of document entity identification"; }

	@Override
	public String version() { return "0.0"; }
	

	/* An empty default constructor is required to comply with BOECLIService */
	
	public TypesIdentification() { }

	@Override
	public final boolean run() {
		
		try {
			
			yyreset(new StringReader(getEngineDocument().getAnnotatedText()));
			yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}
		
		return true;
	}

	/* Use the auxiliary methods provided by the EntityIdentificationService to save the identified entities. */

	private void save(Type type) {
	
		String text = yytext();
		
		addText(text.substring(0,1));
		
		addType(type, text.substring(1, text.length()-1));
		
		yypushback(1);
		
	}
	
%} 


/* Identify entities with a JFlex scanner */


LAW = (law)

JUDGMENT = (judgment)


%x BE   

%%



{NAD}{LAW}{NAD}			{ 
							save(CommonLegislationTypes.LAW);
						}

{NAD}{JUDGMENT}{NAD}	{ 
							save(CommonCaseLawTypes.JUDGMENT);
						}



{BOPEN}		{ 
				addText(yytext()); 
				yybegin(BE); 
			}

<BE> {

	{BCLOS}			{ 
						addText(yytext()); 
						yybegin(YYINITIAL); 
					}
	
	[^]				{ 
						addText(yytext()); 
					}
}

[^]					{ addText(yytext()); }


