package xy.boecli.impl.recognition;

import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.reference.LegalReference;
import eu.boecli.service.impl.recognition.DefaultCaseLawReferenceRecognition;
import xy.boecli.impl.Config;

public class NationalCaseLawReferenceRecognition extends DefaultCaseLawReferenceRecognition {

  	@Override
	public Language language() { return Config.LANG; }

	@Override
	public Jurisdiction jurisdiction() { return Config.JUR; }

	@Override
	public String author() { return Config.AUTHOR; }

	@Override
	public String extensionName() { return Config.NAME; }

	@Override
	public String serviceName() { return "National case-law reference recognition"; }

	@Override
	public String version() { return "0.0"; }

	@Override
	public boolean highPriority() {

		return true;
	}
	
	@Override
	protected final void addCustomFeatures(LegalReference legalReference) {

	}
	
	@Override
	protected void addCommonFeatures(LegalReference legalReference) {

		//Enable/disable here the add of common features depending on specific situations
		
		super.addCommonFeatures(legalReference);
	}

}
